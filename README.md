## 项目简介

这是xolox写的针对vim的lua开发插件，但是[原插件](https://github.com/xolox/vim-lua-ftplugin)不支持openresty的lua模块补全功能，我改了下，增加了针对openresty的配置说明

## 安装

### 用vundle安装

- vimrc中增加

``````
Plugin 'xolox/vim-misc'
Plugin 'https://git.oschina.net/iamdsy/vim-lua-ftplugin'

:let g:lua_interpreter_path = '/usr/local/openresty/bin/resty'
:let $LUA_PATH = '/usr/local/openresty/lualib/resty/?.lua'
:let $LUA_CPATH = '/usr/local/openresty/lualib/?.so'
:let g:lua_complete_omni = 1 "支持c-x c-o快捷键
set completeopt=longest,menu
``````

- 执行如下命令

``````
:source %
:PluginInstall
``````

## 使用
- vim test.lua就可以自动补全了，具体效果见附图

![输入图片说明](http://git.oschina.net/uploads/images/2016/1205/164613_e4b6d01c_607081.png "在这里输入图片标题")

## 前提条件
- 仅能在linux下使用，windows下面使用有问题，原因可能是因为windows版本的resty执行lua时总要生成resty-cli-temp文件夹
- openresty版本：1.11.2.1
- 其他的，不需要了，不需要ctags，不需要vim有+lua特性
- 有问题，请联系我 <iamdsy1@126.com>

## 英文版readme
- 看原作者的[readme](https://github.com/xolox/vim-lua-ftplugin/blob/master/README.md)

